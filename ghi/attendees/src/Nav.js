function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Conference GO!
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <a className="nav-link" href="#">
              Home <span className="sr-only"></span>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="new-location.html">
              New Location
            </a>
          </li>

          <li className="nav-item">
            <a className="nav-link" href="new-conference.html">
              New Conference
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link disabled" href="#">
              New Presentation
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
