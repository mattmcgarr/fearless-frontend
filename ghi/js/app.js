// window.addEventListener("DOMContentLoaded", async () => {
//   const url = "http://localhost:8000/api/conferences/";

//   try {
//     const response = await fetch(url);

//     if (!response.ok) {
//       // Figure out what to do when the response is bad
//     } else {
//       const data = await response.json();
//       const conference = data.conferences[0];

//       //   <h5 class="card-title">Conference name</h5>

//       const nameTag = document.querySelector(".card-title");
//       nameTag.innerHTML = conference.name;

//       //   <p class="card-text">Conference description</p>
//       const detailUrl = `http://localhost:8000${conference.href}`;
//       const detailResponse = await fetch(detailUrl);
//       if (detailResponse.ok) {
//         const details = await detailResponse.json();

//         const description = details.conference.description;
//         const descTag = document.querySelector(".card-text");
//         descTag.innerHTML = conference.description;
//       }
//     }
//   } catch (e) {
//     // Figure out what to do if an error is raised
//   }
// });

function createCard(name, description, pictureUrl, starts, ends) {
  return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
      </div>

      </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends;

          const html = createCard(title, description, pictureUrl);
          const column = document.querySelector(".col");
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
  }
});
