window.addEventListener("DOMContentLoaded", async () => {
  // get locations
  // define URL for API endpoint that returns list of locations
  const url = "http://localhost:8000/api/locations/";
  // fetch function sends a GET request to the endpoint
  // and returns a promise that resolves to the server's response
  // await pauses execution until response is received
  const response = await fetch(url);

  //checks if response from the server is okay
  if (response.ok) {
    // extracts the JSON from the response body
    // also returns a promise
    const data = await response.json();
    console.log(data);
    // gets a reference to a select element in the HTML
    // with an ID of location
    const selectTag = document.getElementById("location");
    // loop iterates over each location in data object
    // data.locations is an array of location objects
    // that were extracted from the response JSON
    for (let location of data.locations) {
      // for each location, a new option is created
      // its value is set to the location's id
      // innerHTML set to location name
      // dynamically populates the select element with options
      // based on data returned from API endpoint
      const option = document.createElement("option");
      option.value = location.id;
      option.innerHTML = location.name;
      selectTag.appendChild(option);
    }
  }

  // reference to a HTML form element with id
  const formTag = document.getElementById("create-conference-form");
  console.log(formTag);

  // add event listener for submit event, fires when user submits form
  // this block of code gets a reference for element, listens
  // for a submit event on the form, then extracts the values
  // as a JSON string
  formTag.addEventListener("submit", async (event) => {
    // prevents the default behavior, which is to reload the page
    event.preventDefault();

    // creates new FormData object from form element
    // collects values from all form fields in HTML and
    // makes them available
    const formData = new FormData(formTag);
    console.log(formData);

    // creates a JSON string from the FormData object by converting
    // it to an array of key-value pairs
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);

    // following code block sends a POST to an API to create
    // a new conference, resets form, and extracts new conference
    // data from server's response

    // defines Url for API endpoint used to create new conference
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    // create config object for fetch()
    // specifies POST request, JSON
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    // sends post request to API endpoint conferenceUrl
    // with config specified above
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      // resets form
      formTag.reset();
      // extracts JSON from response body using
      const newConference = await response.json();
    }
  });
});
